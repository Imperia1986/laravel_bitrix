<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Favorite;
use App\Models\IblockElements;
use App\Services\BitrixProductService;

class ProductsController extends Controller
{

    protected $bitrixService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BitrixProductService $bitrixService)
    {
        $this->middleware('auth');
        $this->bitrixService = $bitrixService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $products = $this->bitrixService->products();

        $favorites = Favorite::where('user_id', Auth::id())->get()->pluck('product_id')->toArray();

        $title = 'Товары';

        return view('products.index', compact('products', 'favorites', 'title'));
    }

    /**
     * Favorite a particular post
     *
     * @param  Post $post
     * @return Response
     */
    public function addFavorite(IblockElements $id)
    {
        Auth::user()->favorites()->attach($id->ID);

        return back();
    }

    /**
     * Unfavorite a particular post
     *
     * @param  Post $post
     * @return Response
     */
    public function removeFavorite(IblockElements $id)
    {
        Auth::user()->favorites()->detach($id->ID);

        return back();
    }
}
