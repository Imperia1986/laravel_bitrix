<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\BitrixProductService;

class UsersController extends Controller
{

    protected $bitrixService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BitrixProductService $bitrixService)
    {
        $this->middleware('auth');
        $this->bitrixService = $bitrixService;
    }

    /**
     * Get all favorite products by user
     *
     * @return Response
     */
    public function myFavorites()
    {
        $favorites = Auth::user()->favorites()->pluck('product_id')->toArray();

        if(sizeof($favorites) > 0){
            $products = $this->bitrixService->products($favorites);
        } else {
            $products = [];
        }

        $title = 'Избранные товары';

        return view('products.index', compact('products', 'favorites', 'title'));
    }
}
