<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BitrixServiceProvider extends ServiceProvider
{
    public function boot()
    {
        require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
    }
}
