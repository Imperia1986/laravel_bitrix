<?php

namespace App\Services;

use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Catalog\ProductTable;
use Bitrix\Catalog\PriceTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\ElementPropertyTable;

class BitrixProductService
{
    /**
     * get products from bitrix
     *
     * @param string $address
     *
     * @return mixed
     */
    public function products($products = [])
    {

        $query = new Query( ProductTable::getEntity() );

        if(sizeof($products) > 0) {
            $query->whereIn("ID",  $products);
        }

        return $query
            ->setSelect(['ID', 'AVAILABLE'])
            ->registerRuntimeField('PRICE', [
                    'data_type' => PriceTable::getEntity(),
                    'reference' => [
                        '=this.ID' => 'ref.PRODUCT_ID'
                    ]
                ]
            )
            ->addSelect('PRICE.PRICE','PRODUCT_PRICE')
            ->registerRuntimeField('ELEMENT', [
                'data_type' => ElementTable::getEntity(),
                'reference' => [
                    '=this.ID' => 'ref.ID'
                    ]
                ]
            )
            ->addFilter("ELEMENT.IBLOCK_ID", config('bitrix.iblockId'))
            ->addSelect('ELEMENT.ID','ELEMENT_ID')
            ->addSelect('ELEMENT.NAME','NAME')
            ->addFilter('ELEMENT.ACTIVE' , 'Y')
             
            ->registerRuntimeField('PROPERTY', [
                'data_type' => ElementPropertyTable::getEntity(),
                'reference' => [
                    '=this.ID' => 'ref.IBLOCK_ELEMENT_ID'
                    ]
                ]
            )
            ->addFilter("PROPERTY.IBLOCK_PROPERTY_ID", config('bitrix.property_material'))
            ->addSelect('PROPERTY.VALUE','material')
            ->setLimit(10)
            ->fetchAll();
    }
}
