<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class IblockElements extends Model
{
    protected $table = 'b_iblock_element';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ID', 'NAME'
    ];
}
