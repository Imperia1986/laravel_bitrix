<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', 'App\Http\Controllers\ProductsController@index');

Route::post('favorite/{id}', 'App\Http\Controllers\ProductsController@addFavorite');
Route::post('unfavorite/{id}', 'App\Http\Controllers\ProductsController@removeFavorite');

Route::get('my_favorites', 'App\Http\Controllers\UsersController@myFavorites')->middleware('auth');
