@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="page-header">
                <h3>{{ $title }}</h3>
            </div>
            @forelse ($products as $product)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $product['NAME'] }}
                    </div>

                    <div class="panel-body">
                        <div>Материал: <i>{{ $product['material'] }}</i></div>
                        <p>{{ $product['PRODUCT_PRICE'] }}</p>
                    </div>
                    @if (Auth::check())
                        <div class="panel-footer">
                            <favorite
                                :product={{ $product['ID'] }}
                                :favorited={{ (in_array($product['ID'], $favorites) ? 'true' : 'false') }}
                            ></favorite>
                        </div>
                    @endif
                </div>
            @empty
                <p>Нет товаров для отображения.</p>
            @endforelse
        </div>
    </div>
</div>
@endsection
